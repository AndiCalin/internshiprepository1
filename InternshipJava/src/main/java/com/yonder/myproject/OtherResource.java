package com.yonder.myproject;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;


@Path("hello")
public class OtherResource {
	
	@GET
    @Produces(MediaType.APPLICATION_JSON)
    public String getItNow() {
        return "Hello again";
    }
	@Path("hello2")
	@GET
    @Produces(MediaType.APPLICATION_JSON)
	public Response getItNow2() {
		return Response.status(999).entity("message : hello").build();
    }
}