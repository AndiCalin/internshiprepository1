package com.yonder.myproject;

import java.util.*;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

//import com.sun.org.apache.xerces.internal.util.Status;
import com.yonder.model.Post;
import com.yonder.repository.*;

@Path("posting")
public class PostingResource {
	
	@Path("postingstring")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response getEmAll() {
		PostRepository Poster = new PostRepository();
		ArrayList<Post> repo = Poster.getAll();
		String str = "";
		for (Post post : repo) {
			str += post.toString();
		}
		return Response.status(23).entity(str).build();
	}
/*	@Path("postinglist")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public ArrayList<Post> getEmAlllist()
	{
		PostRepository Posts = new PostRepository();
		return Posts.getAll(); 
	} */
	@Path("poster/{id}/{description}/{author}")
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	public void postEm(String id, String description, String author)
	{
		PostRepository Posts = new PostRepository();
		Posts.addPost(new Post(id, description, author));
	}	
}