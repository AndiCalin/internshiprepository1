package com.yonder.repository;

import java.util.*;
import com.yonder.model.Post;

public class PostRepository {
	
	public ArrayList<Post> Posts;
	
	public PostRepository() {
		Posts = new ArrayList<Post>();
		addPost(new Post("100", "Description 1", "Somebody13"));
		addPost(new Post("101", "Description 2", "Somebody55"));
		addPost(new Post("102", "Description 3", "Somebody28"));
	}
	
	public ArrayList <Post> getAll()
	{
		return this.Posts;
	}
	
	public void addPost(Post x) {
		Posts.add(x);
	}
	
	public void removePost(Post x) {
		Posts.remove(x);
	}

}
