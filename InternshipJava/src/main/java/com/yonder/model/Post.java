package com.yonder.model;

public class Post {
	String id;
	String description;
	String author;
	
	public Post(String id, String description, String author){
		this.id = id;
		this.description = description;
		this.author = author;
	}
	
	public String getId()
	{
		return this.id;
	}
	
	public String getDescription()
	{
		return this.description;
	}
	
	public String getAuthor()
	{
		return this.author;
	}

	@Override
	public String toString() {
		return "Post [id=" + id + ", description=" + description + ", author=" + author + "]\n";
	}
}