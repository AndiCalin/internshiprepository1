import { Component, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class AppComponent {
  title = 'app';
  pretitle : string = 'Welcome to the one real biggest ';
  description : string = 'Here are some links to help you start right now:';
  date : string = 'Texty text again lol';
  date2 : string = 'Texty text again hahahahahaha';
}
